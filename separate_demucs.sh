# Usage example: ./separate_demucs.sh ./audio/input/amy_13.wav
audio=$1
header="./"
audio=${audio//$header/}
echo "Use demucs to do source separation on $audio"

cd demucs
python -m demucs.separate -o ../audio/output --dl ../$audio
cd ..

