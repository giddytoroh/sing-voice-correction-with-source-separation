import numpy as np
from segmentation_util.MelodyExt import feature_extraction
from segmentation_util.test import inference
from segmentation_util.utils import load_model, matrix_parser

def f0_extraction(x, sr, t_hop=5, *, model_name):
    feature = feature_extraction(x, sr, t_hop)
    t = feature[4] / sr
    feature = np.transpose(feature[0:4], axes=(2, 1, 0))
    model = load_model(model_name)
    extract_result = inference(feature=feature[:, :, 0], model = model, batch_size=32)
    f0 = matrix_parser(extract_result)[:,1]
    f0 = f0.astype(float)
    t = t.astype(float)
    return f0, t