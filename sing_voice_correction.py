import argparse
import numpy as np
import pyworld as pw
import soundfile as sf

from alignment import ctw
from util_data import load_audio
from util_warpath import gen_seq_acc_wp
from feature_extract import pyworld_extract, mceptral

parser = argparse.ArgumentParser(description="Sing Voice Correction")
parser.add_argument("source_file", type=str)
parser.add_argument("target_file", type=str)
parser.add_argument("-o", "--output_file", type=str, default="./a.wav")
parser.add_argument("-f0", "--f0_method", type=str, default="pyworld")
args = parser.parse_args()

sample_rate = 16000
source_vocals, source_bgm = load_audio(args.source_file, False, sr=sample_rate)
target_vocal1, target_vocal2 = load_audio(args.target_file, False, sr=sample_rate)
target_vocals = (target_vocal1 + target_vocal2) / 2

source_f0, source_sp, source_ap = pyworld_extract(source_vocals, sr=sample_rate, f0_method=args.f0_method)
target_f0, target_sp, target_ap = pyworld_extract(target_vocals, sr=sample_rate, f0_method=args.f0_method)
source_feat = mceptral(source_sp)
target_feat = mceptral(target_sp)

cca, lp, ll = ctw(source_feat, target_feat, init='dtw', verbose=True)
wp = lp[-1]
warp_sp = gen_seq_acc_wp(source_sp, wp)
warp_ap = gen_seq_acc_wp(source_ap, wp)
new_audio = pw.synthesize(target_f0, warp_sp, warp_ap, sample_rate, 5)
if len(new_audio) > len(source_bgm):
    new_audio = new_audio[:len(source_bgm)]
elif len(new_audio) < len(source_bgm):
    new_audio = np.append(new_audio, np.zeros(len(source_bgm) - len(new_audio)))

sf.write(args.output_file, np.vstack((source_bgm, new_audio)).T, sample_rate)
