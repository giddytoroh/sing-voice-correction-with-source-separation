# This program will mix 2 channels of input audio into mono channel,
# then clone the channel and make a new 2-channel mixed wav file.

import numpy as np
import argparse
import soundfile

parser = argparse.ArgumentParser(description="Mix stereo audio")
parser.add_argument("input_file", type=str)
parser.add_argument("output_file", type=str)
args = parser.parse_args()

data, sr = soundfile.read(args.input_file)
if len(data.shape) == 2 and data.shape[1] == 2:
    data = np.mean(data, axis=1)
else:
    raise NotImplementedError("Only stereo audio is accepted.")
data = np.vstack((data, data)).T
soundfile.write(args.output_file, data, sr)
