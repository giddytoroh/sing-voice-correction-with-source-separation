# Usage example: ./separate_umxhq.sh ./audio/input/amy_13.wav

# get audio name without extension and make output directory
audio=$(basename -- "$1")
audio="${audio%.*}"
mkdir -p ./audio/output/umxhq/$audio
output="../audio/output/umxhq/$audio"

audio=$1
header="./"
audio=${audio//$header/}
echo "Use umxhq to do source separation on $audio"

cd open-unmix-pytorch
python test.py --model umxhq --outdir $output ../$audio
cd ..

