# Usage example: ./ctw_alignment.sh ./audio/input/amy_13.wav ./audio/input/amy_13_real_stereo.wav
# Source separation of target file is done in advance by separate_demucs.sh and separate_umxhq.sh. 
audio1=$(basename -- "$1")
audio1="${audio1%.*}"
audio2=$(basename -- "$2")
audio2="${audio2%.*}"
echo "Source file: $audio1.wav. Target file: $audio2.wav."

python sing_voice_correction.py -f0 seg -o ./audio/output/$audio2-umxhq-seg.wav $1 ./audio/output/umxhq/$audio2/vocals.wav
python sing_voice_correction.py -f0 crepe -o ./audio/output/$audio2-umxhq-crepe.wav $1 ./audio/output/umxhq/$audio2/vocals.wav
python sing_voice_correction.py -f0 pyworld -o ./audio/output/$audio2-umxhq-pyworld.wav $1 ./audio/output/umxhq/$audio2/vocals.wav
python sing_voice_correction.py -f0 patchcnn -o ./audio/output/$audio2-umxhq-patchcnn.wav $1 ./audio/output/umxhq/$audio2/vocals.wav

python sing_voice_correction.py -f0 seg -o ./audio/output/$audio2-demucs-seg.wav $1 ./audio/output/demucs/$audio2/vocals.wav
python sing_voice_correction.py -f0 crepe -o ./audio/output/$audio2-demucs-crepe.wav $1 ./audio/output/demucs/$audio2/vocals.wav
python sing_voice_correction.py -f0 pyworld -o ./audio/output/$audio2-demucs-pyworld.wav $1 ./audio/output/demucs/$audio2/vocals.wav
python sing_voice_correction.py -f0 patchcnn -o ./audio/output/$audio2-demucs-patchcnn.wav $1 ./audio/output/demucs/$audio2/vocals.wav


