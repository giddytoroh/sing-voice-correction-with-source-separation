"""

Usage examples:

(Use MIR-1k ground truth)
python f0_acc_eval.py -m ./P/A/T/H/MIR-1k -gt abjones_3_06 -f0 crepe ./P/A/T/H/vocals.wav

(Use pseudo label of reference audio as ground truth)
python f0_acc_eval.py -pl crepe -gt ./P/A/T/H/vocals-A.wav -f0 crepe ./P/A/T/H/vocals-B.wav

f0 method and pseudo label should be one of
{pyworld, crepe, patchcnn, seg, seg_timepnn, seg_notepnn}.

"""

import argparse
import librosa
import numpy as np
from os.path import isdir, join
from mir_eval.melody import *
from segmentation_util.utils import midi2freq, freq2midi

def f0_pyworld_method(audio, sr, t_hop):
    from feature_extract import f0_pw_method
    return f0_pw_method(audio, sr, 74, 740, t_hop)

def f0_patchcnn_method(audio, sr, t_hop):
    from f0_patchCNN import f0_extraction as f0_pcnn_method
    return f0_pcnn_method(audio, sr, t_hop)

def f0_crepe_method(audio, sr, t_hop):
    import crepe
    t, f0, cfd, actv = crepe.predict(audio, sr, step_size=t_hop, verbose=0)
    return f0, t

def f0_crepe_0point5_method(audio, sr, t_hop):
    import crepe
    t, f0, cfd, actv = crepe.predict(audio, sr, step_size=t_hop, verbose=0)
    f0[np.where(cfd<0.5)] = 0
    return f0, t

def f0_crepe_otsu_method(audio, sr, t_hop):
    import crepe
    from skimage import filters
    t, f0, cfd, actv = crepe.predict(audio, sr, step_size=t_hop, verbose=0)
    cfd_threshold = filters.threshold_otsu(cfd, nbins=1024)
    f0[np.where(cfd<cfd_threshold)] = 0
    return f0, t

def f0_seg_method_seg(audio, sr, t_hop):
    from f0_segmentation import f0_extraction as f0_seg_method
    return f0_seg_method(audio, sr, t_hop, model_name="Seg")

def f0_seg_method_seg_timepnn(audio, sr, t_hop):
    from f0_segmentation import f0_extraction as f0_seg_method
    return f0_seg_method(audio, sr, t_hop, model_name="time_PNN")

def f0_seg_method_seg_notepnn(audio, sr, t_hop):
    from f0_segmentation import f0_extraction as f0_seg_method
    return f0_seg_method(audio, sr, t_hop, model_name="note_PNN")

f0_methods = {
    "pyworld": f0_pyworld_method,
    "crepe": f0_crepe_method,
    "crepe_0point5": f0_crepe_0point5_method,
    "crepe_otsu": f0_crepe_otsu_method,
    "patchcnn": f0_patchcnn_method,
    "seg": f0_seg_method_seg,
    "seg_timepnn": f0_seg_method_seg_timepnn,
    "seg_notepnn": f0_seg_method_seg_notepnn,
}
sample_rate = 16000
step_size = 20 # millisecond

parser = argparse.ArgumentParser(description="F0 evaluation.")
parser.add_argument("-m", "--MIR1K_path", type=str, default="./MIR-1k")
parser.add_argument("-gt", "--ground_truth", type=str, default=None)
parser.add_argument("-pl", "--pseudo_label", type=str, default=None)
parser.add_argument("-f0", "--f0_method", type=str, default="crepe")
parser.add_argument("input_file", type=str)
args = parser.parse_args()

gt_file = args.ground_truth
ps_label = args.pseudo_label
f0_method = args.f0_method.lower()
# Windows compatiblility
mir1k_path = args.MIR1K_path.replace("\\", "/")
input_file = args.input_file.replace("\\", "/")

# parse ground truth data
if ps_label is None:
    if not isdir(mir1k_path):
        raise ValueError("MIR 1K dataset is not given!")
    if gt_file is None:
        gt_file = input_file.split('/')[-1].split('-')[0]
    print("Ground truth file:", gt_file)
    gt_pitch_label = np.loadtxt(join(mir1k_path, "PitchLabel", gt_file+".pv"))
    gt_voice_label = np.loadtxt(join(mir1k_path, "vocal-nonvocalLabel", gt_file+".vocal"))
else:
    if ps_label.lower() not in f0_methods:
        raise ValueError("Pseudo label is not defined.")
    if gt_file is None:
        raise ValueError("Reference for pseudo label is not given.")
    print("Ground truth audio:", gt_file, "Pseudo label:", ps_label)
    gt_audio, _ = librosa.load(gt_file, sr=sample_rate, mono=True)
    gt_audio = gt_audio.astype(float)
    gt_pitch_label, _ = f0_methods[ps_label.lower()](gt_audio, sample_rate, step_size)
    gt_pitch_label = np.where(gt_pitch_label>0, freq2midi(gt_pitch_label), 0)
    gt_voice_label = (gt_pitch_label > 0).astype(int)

audio, _ = librosa.load(args.input_file, sr=sample_rate, mono=True)
audio = audio.astype(float)
test_pitch, _ = f0_methods[f0_method](audio, sample_rate, step_size)
test_pitch = np.where(test_pitch>0, freq2midi(test_pitch), 0)
test_voice = (test_pitch > 0).astype(int)

# make length same as ground truth
gt_length = len(gt_pitch_label)
if len(test_pitch) < gt_length:
    test_pitch = np.hstack((test_pitch, np.zeros(gt_length-len(test_pitch))))
    test_voice = np.hstack((test_voice, np.zeros(gt_length-len(test_voice))))
else:
    test_pitch = test_pitch[:gt_length]
    test_voice = test_voice[:gt_length]

gt_pitch_cent = gt_pitch_label * 100
test_pitch_cent = test_pitch * 100
result = {}
result["VR"], result["VFA"] = voicing_measures(gt_voice_label, test_voice)
result["RPA"] = raw_pitch_accuracy(gt_voice_label, gt_pitch_cent, test_voice, test_pitch_cent)
result["RCA"] = raw_chroma_accuracy(gt_voice_label, gt_pitch_cent, test_voice, test_pitch_cent)
result["OA"] = overall_accuracy(gt_voice_label, gt_pitch_cent, test_voice, test_pitch_cent)
print(result)

