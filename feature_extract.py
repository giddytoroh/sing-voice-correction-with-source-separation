"""
feature extraction
"""
import numpy as np
import pyworld as pw
import pysptk as ps
import librosa
from sklearn.preprocessing import StandardScaler

def f0_pw_method(audio, sr, f0_low, f0_high, t_hop):
    f0, t = pw.harvest(audio, sr, f0_floor=f0_low, f0_ceil=f0_high, frame_period=t_hop)
    f0 = pw.stonemask(audio, f0, t, sr)
    return f0, t

def pyworld_extract(audio, sr=16000, f0_low=74, f0_high=740, \
                    n_fft=1024, t_hop=5, *, f0_method="pyworld"):
    """extract WORLD parameters"""
    audio = audio.astype(float)
    f0_method = f0_method.lower()
    if f0_method == "pyworld":
        print("Use PyWorld f0 extraction.")
        f0, t = f0_pw_method(audio, sr, f0_low, f0_high, t_hop)
    elif f0_method == "patchcnn":
        from f0_patchCNN import f0_extraction as f0_pcnn_method
        print("Use patch CNN f0 extraction.")
        f0, t = f0_pcnn_method(audio, sr, t_hop)
    elif f0_method.startswith("seg"):
        from f0_segmentation import f0_extraction as f0_seg_method
        if f0_method == "seg":
            print("Use semantic segmentation f0 extraction.")
            f0, t = f0_seg_method(audio, sr, t_hop, model_name="Seg")
        elif f0_method == "seg_timepnn":
            print("Use semantic segmentation f0 extraction with time PNN.")
            f0, t = f0_seg_method(audio, sr, t_hop, model_name="time_PNN")
        elif f0_method == "seg_notepnn":
            print("Use semantic segmentation f0 extraction with note PNN.")
            f0, t = f0_seg_method(audio, sr, t_hop, model_name="note_PNN")
        else:
            raise NotImplementedError
    elif f0_method == "crepe":
        import crepe
        print("Use crepe f0 extraction.")
        t, f0, cfd, actv = crepe.predict(audio, sr, step_size=t_hop, viterbi=True)
    elif f0_method.startswith("crepe_"):
        import crepe
        print("Use crepe f0 extraction.")
        if f0_method == "crepe_0point5":
            print("Threshold is 0.5.")
            t, f0, cfd, actv = crepe.predict(audio, sr, step_size=t_hop, viterbi=True)
            f0[np.where(cfd<0.5)] = 0
        elif f0_method == "crepe_otsu":
            from skimage import filters
            t, f0, cfd, actv = crepe.predict(audio, sr, step_size=t_hop, viterbi=True)
            cfd_threshold = filters.threshold_otsu(cfd, nbins=1024)
            print("Threshold is {}.".format(cfd_threshold))
            f0[np.where(cfd<cfd_threshold)] = 0
        else:
            raise NotImplementedError
    else:
        raise NotImplementedError
    ap = pw.d4c(audio, f0, t, sr, fft_size=n_fft)
    sp = pw.cheaptrick(audio, f0, t, sr, f0_floor=f0_low, fft_size=n_fft)
    return f0, sp, ap

def mceptral(spec, order=24, alpha=0.35, norm=True):
    """
    generate mcep from spectrogram;
    input spec shape in [n_sample, n_dim]
    """
    mcep = np.array([ps.sp2mc(X_i, order, alpha) for X_i in spec])
    if norm:
        scaler = StandardScaler()
        mcep = scaler.fit_transform(mcep)
    return mcep

def logspec(audio, sr=16000, n_fft=1024, hop_size=80, n_band=64, logmag=True):
    """generate log-scaled, log-magnituded spectrum"""

    S = librosa.feature.melspectrogram(y=audio, sr=sr, n_fft=n_fft, \
                                       hop_length=hop_size, n_mels=n_band)
    if logmag:
        S = librosa.core.power_to_db(S)
    return S